import kotlin.math.pow

fun digPow(n: Int, p: Int): Int {
    val result = n.toString().split("").filter(String::isNotEmpty).map(String::toInt).mapIndexed {i, num -> num.toDouble().pow(i + p).toInt() }.sum()
    return when (result.rem(n)) {
        0 -> result.div(n)
        else -> -1
    }
}

fun main() {
    println(digPow(89,1))
    println(digPow(695,2))
    println(digPow(46288,3))
}